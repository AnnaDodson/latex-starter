#!/usr/bin/env bash

usage () {
		echo -en "\033[93m"
		cat << USAGESTRING
setup.sh [document-type] [document-name]
	document-type = [document]
	For example, \`setup.sh document psyjvta-dissertation\`
USAGESTRING
		echo -en "\033[0m"
}

error () {
		echo -e "\033[91mError: $@\033[0m"
}

fail () {
		error "$@"
		usage
		exit 1
}

# http://stackoverflow.com/a/246128
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [[ -z "$2" ]];
then
		fail "No document name provided"
fi

DOCUMENT_NAME="$2"

case "$1" in
		document)
				if [[ -e ".gitignore" ]];
				then
						cat "$DIR/TeX.gitignore" >> .gitignore
				else
						cp "$DIR/TeX.gitignore" .gitignore
				fi
				cp "$DIR/document.tex" "${DOCUMENT_NAME}.tex"
				touch "$DOCUMENT_NAME.bib"
				cp "$DIR/Makefile" .
				sed -i "s/REPLACEME/${DOCUMENT_NAME}/g" Makefile "${DOCUMENT_NAME}.tex"
				;;
		usage)
				usage
				;;
		*)
				fail "No valid option specified"
				;;
esac
