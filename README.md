# LaTeX Starter

The starter template for my LaTeX documents.

For instance, to create a document called `psyjvta-dissertation.pdf`, you would run the following:

```bash
git clone $repo-url latex-starter
./latex-starter/setup.sh document psyjvta-dissertation
```

## Using Submodules

In order to track that you are using the `latex-starter` repo, you will need to use Git submodules:

```bash
git submodule init
git submodule add https://gitlab.com/jamietanna/latex-starter.git
```

You will notice a new file `.gitmodules` that you will need to add and commit to your repo.

When cloning your own repo, you will find that the submodules are _not_ downloaded as well as your repo. To do this, you will need to run the following:

```bash
git submodule init
git submodule update
```

Alternatively, you can run the following when cloning **your own repo**:

```bash
git clone --recursive $your-git-repo
```

References:
- https://www.git-scm.com/book/en/v2/Git-Tools-Submodules

## TODOs

- Different preambles when i.e.
	- including code
	- writing Beamer presentations
- What about overwriting files?
